FROM openjdk:11 as build

COPY . .

WORKDIR /platform/quarkus

RUN ../.././gradlew assemble -Ptriplestore

FROM openjdk:11-jre-slim

ENV JAVA_OPTIONS="-Dquarkus.http.host=0.0.0.0 -Djava.util.logging.manager=org.jboss.logmanager.LogManager -XX:+ExitOnOutOfMemoryError -Dcom.github.jsonldjava.disallowRemoteContextLoading=true"

COPY --from=build /platform/quarkus/build/quarkus-app/lib/ /trellis/lib/
COPY --from=build /platform/quarkus/build/quarkus-app/app/ /trellis/app/
COPY --from=build /platform/quarkus/build/quarkus-app/quarkus/ /trellis/quarkus/
COPY --from=build /platform/quarkus/build/quarkus-app/quarkus-run.jar /trellis/app.jar

WORKDIR /trellis/

EXPOSE 8253

ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar app.jar"]
